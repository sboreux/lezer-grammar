# lezer-grammar

This is a lezer-grammar grammar for the [lezer](https://lezer.codemirror.net/) parser system.

Reference:
- https://lezer.codemirror.net/docs/guide/