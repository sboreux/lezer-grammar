const {parser} = require("../dist/index");
const {fileTests} = require ("lezer-generator/dist/test")

let fs = require("fs"), path = require("path")
let caseDir = __dirname

for (let file of fs.readdirSync(caseDir)) {
  if (file === 'test-lezer.js') continue
  let re = /^[^\.]*/;
  let result = re.exec(file);
  let name = result[0];

  describe(name, () => {
    for (let { name, run } of fileTests(fs.readFileSync(path.join(caseDir, file), "utf8"), file))
      it(name, () => run(parser))
  })
}