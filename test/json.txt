# json 

@top JsonText { value }

value { True | False | Null | Number | String | Object | Array }

String { string }
Object { "{" list<Property>? "}" }
Array  { "[" list<value>? "]" }

Property { PropertyName ":" value }
PropertyName { string }


@tokens {
  True  { "true" }
  False { "false" }
  Null  { "null" }

  Number { '-'? int frac? exp?  }
  int  { '0' | $[1-9] std.digit* }
  frac { '.' std.digit+ }
  exp  { $[eE] $[+\-]? std.digit+ }

  string { '"' char* '"' }
  char { $[\u{20}\u{21}\u{23}-\u{5b}\u{5d}-\u{10ffff}] | "\\" esc }
  esc  { $["\\\/bfnrt] | "u" hex hex hex hex }
  hex  { $[0-9a-fA-F] }

  whitespace { $[ \n\r\t] }
}

@skip { whitespace }
list<item> { item ("," item)* }

  @detectDelim

==>

LezerGrammar(
  Top(NonTerminalName,RuleBody(Term(TermName))),

  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName))),
  
  NonTerminal(NonTerminalName,RuleBody(Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(TermName)),Multiplier),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(TermName)),Multiplier),Term(Literal))),
  
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(Literal),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName))),
  
  Tokens(
    Token(TokenName,RuleBody(Term(Literal))),
    Token(TokenName,RuleBody(Term(Literal))),
    Token(TokenName,RuleBody(Term(Literal))),

    Token(TokenName,RuleBody(Term(Literal,Multiplier),Term(TermName),Term(TermName,Multiplier),Term(TermName,Multiplier))),
    Token(TokenName,RuleBody(Term(Literal),Term(CharSet),Term(CharSet,Multiplier))),
    Token(TokenName,RuleBody(Term(Literal)Term(CharSet,Multiplier))),
    Token(TokenName,RuleBody(Term(CharSet),Term(CharSet,Multiplier),Term(CharSet,Multiplier))),

    Token(TokenName,RuleBody(Term(Literal),Term(TermName,Multiplier),Term(Literal))),
    Token(TokenName,RuleBody(Term(CharSet),Term(Literal),Term(TermName))),
    Token(TokenName,RuleBody(Term(CharSet),Term(Literal),Term(TermName),Term(TermName),Term(TermName),Term(TermName))),
    Token(TokenName,RuleBody(Term(CharSet))),

    Token(TokenName,RuleBody(Term(CharSet)))
  )
  Skip(RuleBody(Term(TermName))),
  NonTerminal(NonTerminalName,TemplateArguments(ArgName),RuleBody(Term(TermName),Term(Term(Literal),Term(TermName),Multiplier))),
  
  DetectDelim
)
