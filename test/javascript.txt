# javascript

@precedence {
  else @left,
  member,
  newArgs,
  call,
  taggedTemplate,
  prefix,
  postfix,
  typeof,
  exp @left,
  times @left,
  plus @left,
  shift @left,
  loop,
  rel @left,
  equal @left,
  bitOr @left,
  bitXor @left,
  bitAnd @left,
  and @left,
  or @left,
  ternary @left,
  assign @left,
  comma @left,
  statement @cut
}

@top Script { statement+ }

statement {
  ExportDeclaration |
  ImportDeclaration |
  ForStatement { kw<"for"> ckw<"await">? (ForSpec | ForInSpec | ForOfSpec) statement } |
  WhileStatement { kw<"while"> ParenthesizedExpression statement } |
  WithStatement { kw<"with"> ParenthesizedExpression statement } |
  DoStatement { kw<"do"> statement kw<"while"> ParenthesizedExpression semi } |
  IfStatement { kw<"if"> ParenthesizedExpression statement (!else kw<"else"> statement)? } |
  SwitchStatement { kw<"switch"> ParenthesizedExpression SwitchBody { "{" switchItem* "}" } } |
  TryStatement { kw<"try"> Block (kw<"catch"> ("(" pattern ")")? Block)? (kw<"finally"> Block)? } |
  ReturnStatement { kw<"return"> (noSemi expression)? semi } |
  ThrowStatement { kw<"throw"> expression semi } |
  BreakStatement { kw<"break"> (noSemi Label)? semi } |
  ContinueStatement { kw<"continue"> (noSemi Label)? semi } |
  DebuggerStatement { kw<"debugger"> semi } |
  Block |
  LabeledStatement { Label ":" statement } |
  declaration |
  ExpressionStatement { expression semi } |
  ";"
}

ExportDeclaration {
  kw<"export"> Star ckw<"from"> String semi |
  kw<"export"> kw<"default"> (FunctionDeclaration | ClassDeclaration | expression semi) |
  kw<"export"> declaration |
  kw<"export"> ExportGroup (ckw<"from"> String)? semi
}

ExportGroup {
  "{" commaSep<VariableName (ckw<"as"> VariableName { word })?> "}"
}

ImportDeclaration {
  kw<"import"> (Star ckw<"as"> VariableDefinition | commaSep<VariableDefinition | ImportGroup>) ckw<"from"> String semi |
  kw<"import"> String semi
}

ImportGroup {
  "{" commaSep<VariableDefinition | VariableName ckw<"as"> VariableDefinition> "}"
}

ForSpec {
  "("
  (VariableDeclaration | expression ";" | ";") expression? ";" expression?
  ")"
}

forXSpec<op> {
  "("
  ((kw<"let"> | kw<"var"> | kw<"const">) pattern | VariableName | MemberExpression | ArrayPattern | ObjectPattern)
  !loop op expression
  ")"
}

ForInSpec { forXSpec<kw<"in">> }
ForOfSpec { forXSpec<ckw<"of">> }

declaration {
  FunctionDeclaration | ClassDeclaration | VariableDeclaration
}

FunctionDeclaration {
  async? !statement kw<"function"> Star? VariableDefinition? ParamList Block
}

ClassDeclaration {
  !statement kw<"class"> VariableDefinition (kw<"extends"> expression)? ClassBody
}

ClassBody {
  "{" (MethodDeclaration | ";")* "}"
}

MethodDeclaration {
  pkwMod<"static">?
  pkwMod<"async">?
  (pkwMod<"get"> | pkwMod<"set"> | Star)?
  PropertyNameDefinition
  ParamList
  Block
}

VariableDeclaration {
  (kw<"let"> | kw<"var"> | kw<"const">) commaSep1<patternAssign> semi
}

pattern { VariableDefinition | ArrayPattern | ObjectPattern }

ArrayPattern { "[" commaSep<"..."? patternAssign> ~destructure "]" }

ObjectPattern { "{" commaSep<PatternProperty> ~destructure "}" }

patternAssign {
  pattern ("=" expressionNoComma)?
}

ParamList {
  "(" commaSep<"..." patternAssign | patternAssign> ")"
}

Block {
  !statement "{" statement* "}"
}

switchItem {
  CaseLabel { kw<"case"> expression ":" } |
  DefaultLabel { kw<"default"> ":" } |
  statement
}

expression {
  expressionNoComma | SequenceExpression
}

SequenceExpression {
  expressionNoComma !comma ("," expressionNoComma)+
}

expressionNoComma {
  Number |
  String |
  TemplateString |
  VariableName |
  boolean |
  kw<"this"> |
  kw<"null"> |
  kw<"super"> |
  RegExp |
  ArrayExpression |
  ObjectExpression { "{" commaSep<Property> ~destructure "}" } |
  NewExpression |
  UnaryExpression |
  ParenthesizedExpression |
  ClassExpression |
  FunctionExpression |
  ArrowFunction |
  MemberExpression |
  BinaryExpression |
  ConditionalExpression { expressionNoComma !ternary LogicOp<"?"> expressionNoComma LogicOp<":"> expressionNoComma } |
  AssignmentExpression |
  PostfixExpression { expressionNoComma !postfix PostfixOp } |
  CallExpression { expressionNoComma !call ArgList } |
  TaggedTemplatExpression { expressionNoComma !taggedTemplate TemplateString }
}

ParenthesizedExpression { "(" expression ")" }

ArrayExpression {
  "[" commaSep1<"..."? expressionNoComma | ""> ~destructure "]"
}

propName { PropertyNameDefinition | "[" expression "]" | Number | String }

Property {
  pkwMod<"async">? (pkwMod<"get"> | pkwMod<"set"> | Star)? propName ParamList Block |
  propName ~destructure (":" expressionNoComma)? |
  "..." expressionNoComma
}

PatternProperty {
  "..." patternAssign |
  (PropertyName | Number | String) ~destructure (":" pattern)? ("=" expressionNoComma)?
}

ClassExpression {
  kw<"class"> VariableDefinition? (kw<"extends"> expression)? ClassBody
}

FunctionExpression {
  kw<"function"> Star? VariableDefinition? ParamList Block
}

NewExpression {
  kw<"new"> expressionNoComma (!newArgs ArgList)?
}

UnaryExpression {
  !prefix (ckw<"await"> | ckw<"yield"> | kw<"void"> | kw<"typeof"> | kw<"delete"> |
           LogicOp<"!"> | BitOp<"~"> | ArithOp<"++" | "--"> | ArithOp<"+" | "-">)
  expressionNoComma
}

BinaryExpression {
  expressionNoComma !exp ArithOp<"**"> expressionNoComma |
  expressionNoComma !times (divide | ArithOp<"%"> | ArithOp<"*">) expressionNoComma |
  expressionNoComma !plus ArithOp<"+" | "-"> expressionNoComma |
  expressionNoComma !shift BitOp<">>" ">"? | "<<"> expressionNoComma |
  expressionNoComma !rel (CompareOp<"<" "="? | ">" "="?> | kw<"in"> | kw<"instanceof">) expressionNoComma |
  expressionNoComma !equal CompareOp<"==" "="? | "!=" "="?> expressionNoComma |
  expressionNoComma !bitOr BitOp<"|"> expressionNoComma |
  expressionNoComma !bitXor BitOp<"^"> expressionNoComma |
  expressionNoComma !bitAnd BitOp<"&"> expressionNoComma |
  expressionNoComma !and LogicOp<"&&"> expressionNoComma |
  expressionNoComma !or LogicOp<"||" | "??"> expressionNoComma
}

AssignmentExpression {
  (VariableName | MemberExpression) !assign UpdateOp<($[+\-/|&%^] | "*" "*"? | "<<" | ">>" ">"?) "="> expressionNoComma |
  (VariableName | MemberExpression | ArrayPattern | ObjectPattern) !assign "=" expressionNoComma
}

MemberExpression {
  expressionNoComma !member (("." | "?.") PropertyName | "[" expression "]")
}

ArgList {
  "(" commaSep<"..."? expressionNoComma> ")"
}

ArrowFunction {
  async? (ParamList { VariableDefinition } | ParamList) "=>" (Block | expression)
}

@skip {} {
  TemplateString {
    templateStart (templateContent | templateExpr)* templateEnd
  }
}

templateExpr { templateDollarBrace expression templateClosingBrace }

commaSep<content> {
  "" | content ("," content?)*
}

commaSep1<content> {
  content ("," content)*
}

// Keywords

kw<term> { @specialize[name={term}]<identifier, term> }

// Contextual keywords

ckw<term> { @extend[name={term}]<identifier, term> }

async { @extend[name=async]<identifier, "async"> }

// Contextual keyword in property context

pkwMod<term> { @extend[name={term}]<word, term> }

semi { ";" | insertSemi }

boolean { @specialize[name=BooleanLiteral]<identifier, "true" | "false"> }

Star { "*" }

VariableName { identifier ~arrow }

VariableDefinition { identifier ~arrow }

Label { identifier }

PropertyName { word }

PropertyNameDefinition { word }

@skip { whitespace | LineComment | BlockComment }

@external tokens noSemicolon from "./tokens" { noSemi }

@external tokens postfix from "./tokens" { PostfixOp }

@tokens {
  whitespace { std.whitespace+ }

  LineComment { "//" ![\n]* }

  BlockComment { "/*" blockCommentRest }

  blockCommentRest { ![*] blockCommentRest | "*" blockCommentAfterStar }

  blockCommentAfterStar { "/" | "*" blockCommentAfterStar | ![/*] blockCommentRest }

  divide[name=ArithOp] { "/" }

  @precedence { BlockComment, LineComment, divide }

  @precedence { BlockComment, LineComment, RegExp }

  identifierChar { std.asciiLetter | $[_$\u{a1}-\u{10ffff}] }

  word { identifierChar (identifierChar | std.digit)* }

  identifier { word }

  @precedence { identifier, whitespace }

  @precedence { word, whitespace }

  Number {
    (std.digit+ ("." std.digit*)? | "." std.digit+) (("e" | "E") ("+" | "-")? std.digit+)? |
    "0x" (std.digit | $[a-fA-F])+ |
    "0b" $[01]+ |
    "0o" $[0-7]+
  }

  String {
    '"' (![\\\n"] | "\\" _)* '"'? |
    "'" (![\\\n'] | "\\" _)* "'"?
  }

  templateStart { "`" }

  templateClosingBrace { "}" }

  ArithOp<expr> { expr }
  LogicOp<expr> { expr }
  BitOp<expr> { expr }
  CompareOp<expr> { expr }
  UpdateOp<expr> { expr }

  RegExp { "/" (![/\\\n[] | "\\" ![\n] | "[" (![\n\\\]] | "\\" ![\n])* "]")+ ("/" $[gimsuy]*)? }

  "="[name=Equals]
  "..."[name=Spread]
  "=>"[name=Arrow]

  "(" ")" "[" "]" "{" "}"

  "?." "." "," ";" ":"
}

@external tokens insertSemicolon from "./tokens" { insertSemi }

@external tokens template from "./tokens" {
  templateContent,
  templateDollarBrace,
  templateEnd
}

@detectDelim

==>

LezerGrammar(
  Precedence(
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName),
    PrecedenceItem(PrecedenceName),
    PrecedenceItem(PrecedenceName),
    PrecedenceItem(PrecedenceName),
    PrecedenceItem(PrecedenceName),
    PrecedenceItem(PrecedenceName),
    PrecedenceItem(PrecedenceName),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword),
    PrecedenceItem(PrecedenceName,PrecedenceKeyword)
  ),
  Top(NonTerminalName,RuleBody(Term(TermName,Multiplier))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(TermName),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal)),Multiplier),Term(Term(TermName),Term(TermName),Term(TermName)),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName),Term(Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Multiplier)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,Multiplier),Term(Literal))))))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(Term(Literal),Term(TermName),Term(Literal),Multiplier),Term(TermName),Multiplier),Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Multiplier)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(Term(TermName),Term(TermName),Multiplier),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(Term(TermName),Term(TermName),Multiplier),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(Term(TermName),Term(TermName),Multiplier),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName)))),Term(TermName),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(Literal),Term(TermName)))),Term(TermName),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(TermName)))),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(Term(TermName),Term(TermName),Term(TermName),Term(TermName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Multiplier),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(TermName),Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName)))),Multiplier))),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName,TemplateInstanceArguments(Term(TermName),Term(TermName)))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(TermName),Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName))),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(Term(TermName),Term(TermName),Term(Literal),Term(Literal)),Term(TermName,Multiplier),Term(Literal),Term(TermName,Multiplier),Term(Literal))),
  NonTerminal(NonTerminalName,TemplateArguments(ArgName),RuleBody(Term(Literal),Term(Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal)))),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName)),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName),Term(TermName),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(TermName,TemplateInstanceArguments(Term(Literal))))))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(TermName,TemplateInstanceArguments(Term(Literal))))))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(TermName),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,Multiplier),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,Multiplier),Term(TermName,Multiplier),Term(TermName),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Multiplier),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(Term(TermName),Term(Literal),Multiplier),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal)),Multiplier),Term(TermName,TemplateInstanceArguments(Term(Literal)),Multiplier),Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Multiplier),Term(TermName),Term(TermName),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal)))),Term(TermName,TemplateInstanceArguments(Term(TermName))),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(TermName),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(Literal,Multiplier),Term(TermName))),Term(AmbiguityMarker(MarkerName)),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(TermName))),Term(AmbiguityMarker(MarkerName)),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(Term(Literal),Term(TermName),Multiplier))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(Literal),Term(TermName),Term(TermName))),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(Literal),Term(TermName,Multiplier),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(Literal)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(Literal)))),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(Term(Literal),Term(TermName),Multiplier))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName),Term(NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(TermName))),Term(AmbiguityMarker(MarkerName)),Term(Literal)))),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName)))),Term(TermName),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName)))),Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName)))))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(Literal,Multiplier),Term(TermName),Term(Literal))),Term(AmbiguityMarker(MarkerName)),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(Literal),Term(TermName),Term(Literal),Term(TermName),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal)),Multiplier),Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Multiplier),Term(TermName),Term(TermName),Term(TermName),Term(TermName),Term(AmbiguityMarker(MarkerName)),Term(Term(Literal),Term(TermName),Multiplier),Term(Literal),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName),Term(Term(TermName),Term(TermName),Term(TermName)),Term(AmbiguityMarker(MarkerName)),Term(Term(Literal),Term(TermName),Multiplier),Term(Term(Literal),Term(TermName),Multiplier))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,Multiplier),Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Multiplier),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,Multiplier),Term(TermName,Multiplier),Term(TermName),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName),Multiplier))),
  NonTerminal(NonTerminalName,RuleBody(Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal),Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal),Term(Literal)))),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(Term(TermName),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal)))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal),Term(Literal))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal),Term(Literal,Multiplier),Term(Literal))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(Term(TermName,TemplateInstanceArguments(Term(Literal),Term(Literal,Multiplier),Term(Literal),Term(Literal,Multiplier))),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName,TemplateInstanceArguments(Term(Literal)))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal),Term(Literal,Multiplier),Term(Literal),Term(Literal,Multiplier))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal))),Term(TermName),Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Literal),Term(Literal))),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(Term(TermName),Term(TermName)),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(TermName,TemplateInstanceArguments(Term(Term(CharSet),Term(Literal),Term(Literal,Multiplier),Term(Literal),Term(Literal),Term(Literal,Multiplier)),Term(Literal))),Term(TermName),Term(Term(TermName),Term(TermName),Term(TermName),Term(TermName)),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(Literal),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(PrecedenceMarker(MarkerStart,PrecedenceName)),Term(Term(Term(Literal),Term(Literal)),Term(TermName),Term(Literal),Term(TermName),Term(Literal)))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName,TemplateInstanceArguments(Term(Literal,Multiplier),Term(TermName))),Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName,Multiplier),Term(Term(NonTerminal(NonTerminalName,RuleBody(Term(TermName)))),Term(TermName)),Term(Literal),Term(Term(TermName),Term(TermName)))),
  Skip(RuleBody,NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(Term(TermName),Term(TermName),Multiplier),Term(TermName)))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(TermName),Term(TermName))),
  NonTerminal(NonTerminalName,TemplateArguments(ArgName),RuleBody(Term(Literal),Term(TermName),Term(Term(Literal),Term(TermName,Multiplier),Multiplier))),
  NonTerminal(NonTerminalName,TemplateArguments(ArgName),RuleBody(Term(TermName),Term(Term(Literal),Term(TermName),Multiplier))),
  Comment,
  NonTerminal(NonTerminalName,TemplateArguments(ArgName),RuleBody(Specialize(NodeProp(PropName,ArgName),TokenName,Term(TermName)))),
  Comment,
  NonTerminal(NonTerminalName,TemplateArguments(ArgName),RuleBody(Specialize(NodeProp(PropName,ArgName),TokenName,Term(TermName)))),
  NonTerminal(NonTerminalName,RuleBody(Specialize(NodeProp(PropName,TermName),TokenName,Term(Literal)))),
  Comment,
  NonTerminal(NonTerminalName,TemplateArguments(ArgName),RuleBody(Specialize(NodeProp(PropName,ArgName),TokenName,Term(TermName)))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Specialize(NodeProp(PropName,TermName),TokenName,Term(Literal),Term(Literal)))),
  NonTerminal(NonTerminalName,RuleBody(Term(Literal))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(AmbiguityMarker(MarkerName)))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(AmbiguityMarker(MarkerName)))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName))),
  NonTerminal(NonTerminalName,RuleBody(Term(TermName))),
  Skip(RuleBody(Term(TermName),Term(TermName),Term(TermName))),
  External(SymbolName,Literal,TermName),
  External(SymbolName,Literal,TermName),
  Tokens(
    Token(TokenName,RuleBody(Term(CharSet,Multiplier))),
    Token(TokenName,RuleBody(Term(Literal),Term(CharSet,Multiplier))),
    Token(TokenName,RuleBody(Term(Literal),Term(TermName))),
    Token(TokenName,RuleBody(Term(CharSet),Term(TermName),Term(Literal),Term(TermName))),
    Token(TokenName,RuleBody(Term(Literal),Term(Literal),Term(TermName),Term(CharSet),Term(TermName))),
    Token(TokenName,NodeProp(PropName,TermName),RuleBody(Term(Literal))),TokenPrecedence(TermName,TermName,TermName),TokenPrecedence(TermName,TermName,TermName),
    Token(TokenName,RuleBody(Term(CharSet),Term(CharSet))),
    Token(TokenName,RuleBody(Term(TermName),Term(Term(TermName),Term(CharSet),Multiplier))),
    Token(TokenName,RuleBody(Term(TermName))),TokenPrecedence(TermName,TermName),TokenPrecedence(TermName,TermName),
    Token(TokenName,RuleBody(Term(Term(CharSet,Multiplier),Term(Term(Literal),Term(CharSet,Multiplier),Multiplier),Term(Literal),Term(CharSet,Multiplier)),Term(Term(Term(Literal),Term(Literal)),Term(Term(Literal),Term(Literal),Multiplier),Term(CharSet,Multiplier),Multiplier),Term(Literal),Term(Term(CharSet),Term(CharSet),Multiplier),Term(Literal),Term(CharSet,Multiplier),Term(Literal),Term(CharSet,Multiplier))),
    Token(TokenName,RuleBody(Term(Literal),Term(Term(CharSet),Term(Literal),Term(Any),Multiplier),Term(Literal,Multiplier),Term(Literal),Term(Term(CharSet),Term(Literal),Term(Any),Multiplier),Term(Literal,Multiplier))),
    Token(TokenName,RuleBody(Term(Literal))),
    Token(TokenName,RuleBody(Term(Literal))),
    Token(TokenName,TemplateArguments(ArgName),RuleBody(Term(TermName))),
    Token(TokenName,TemplateArguments(ArgName),RuleBody(Term(TermName))),
    Token(TokenName,TemplateArguments(ArgName),RuleBody(Term(TermName))),
    Token(TokenName,TemplateArguments(ArgName),RuleBody(Term(TermName))),
    Token(TokenName,TemplateArguments(ArgName),RuleBody(Term(TermName))),
    Token(TokenName,RuleBody(Term(Literal),Term(Term(CharSet),Term(Literal),Term(CharSet),Term(Literal),Term(Term(CharSet),Term(Literal),Term(CharSet),Multiplier),Term(Literal),Multiplier),Term(Term(Literal),Term(CharSet,Multiplier),Multiplier))),
    LiteralToken(Literal,NodeProp(PropName,TermName)),
    LiteralToken(Literal,NodeProp(PropName,TermName)),
    LiteralToken(Literal,NodeProp(PropName,TermName)),
    LiteralToken(Literal),
    LiteralToken(Literal),
    LiteralToken(Literal),
    LiteralToken(Literal),
    LiteralToken(Literal),
    LiteralToken(Literal),
    LiteralToken(Literal),
    LiteralToken(Literal),
    LiteralToken(Literal),
    LiteralToken(Literal),
    LiteralToken(Literal)
  ),
  External(SymbolName,Literal,TermName),
  External(SymbolName,Literal,TermName,TermName,TermName),
  DetectDelim
)
