# simple grammar from doc

@top { expression }

expression { Name | Number | BinaryExpression }

BinaryExpression { "(" expression ("+" | "-") expression ")" }

@tokens {
  Name { std.asciiLetter+ }
  Number { std.digit+ }
}

==>

LezerGrammar(Top(RuleBody(Term(TermName))),NonTerminal(NonTerminalName,RuleBody(Term(TermName),Term(TermName),Term(TermName))),NonTerminal(NonTerminalName,RuleBody(Term(Literal),Term(TermName),Term(Term(Literal),Term(Literal)),Term(TermName),Term(Literal))),Tokens(Token(TokenName,RuleBody(Term(CharSet,Multiplier))),Token(TokenName,RuleBody(Term(CharSet,Multiplier)))))
