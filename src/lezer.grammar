@top LezerGrammar { rule+ }

rule {
    Top | NonTerminal | Skip | Tokens | Precedence | DetectDelim | External
}

Top{
    "@top" NonTerminalName? RuleBody
}

NonTerminal {
    NonTerminalName NodeProp? TemplateArguments? RuleBody
}

Skip {
    "@skip" RuleBody ( "{" rule+ "}" )?
}

Precedence {
    "@precedence" "{" PrecedenceItem ("," PrecedenceItem)* "}"
}

PrecedenceItem {
    PrecedenceName PrecedenceKeyword?
}

PrecedenceKeyword {
    "@cut" | "@left" | "@right" 
}

DetectDelim {
    "@detectDelim"
}

External {
    "@external" "tokens" SymbolName "from" Literal "{" TermName ("," TermName)* "}"
}

TemplateArguments {
    "<" ArgName ("," ArgName)* ">"
}

TemplateInstanceArguments {
    "<" (expression) ("," (expression))* ">"
}

NodeProp {
    "[" PropName "=" ("{" ArgName "}" | TermName) "]"
}

RuleBody {
    "{" expression "}"
}


expression {
    Term ("|"? Term )* | Specialize |
}

Specialize {
    ("@specialize" | "@extend") NodeProp? "<" TokenName "," (expression) ">"
}

Term {
    ( TermName TemplateInstanceArguments? | CharSet | Literal  | ( "(" expression ")" | NonTerminal ) ) Multiplier? | marker | Any
}

marker {
    PrecedenceMarker | AmbiguityMarker
}

PrecedenceMarker {
    MarkerStart PrecedenceName
}

AmbiguityMarker {
    "~" MarkerName
}

Tokens {
    "@tokens" "{" tokenRule+ "}"
}

tokenRule {
    Token | TokenPrecedence | LiteralToken
}

Token {
    TokenName NodeProp? TemplateArguments? RuleBody  
}

LiteralToken {
    Literal NodeProp?
}

TokenPrecedence {
    "@precedence" "{" TermName ("," TermName)* "}"
}



SymbolName { identifier }
PropName { identifier }
NonTerminalName { identifier ~inline}
TermName { identifier ~inline}
TokenName { identifier }
ArgName { identifier }
MarkerName { identifier }
PrecedenceName { identifier }

@tokens {
    identifier { std.asciiLetter (std.asciiLetter | std.digit)* }
    Multiplier { $[+*?] }
    CharSet { ( ("$"|"!") "[" ( ![\]\\] | "\\" escCharSet )+ "]" ) | "std.asciiLetter" | "std.asciiLowercase" | "std.asciiUppercase"  | "std.digit" | "std.whitespace"}
    MarkerStart { "!" }
    @precedence { CharSet, identifier , MarkerStart   }
    Literal { '"' ( !["\\] | "\\" escDouble )* '"' | "'" ( !['\\] | "\\" escSimple )* "'"}
    escDouble { esc | '"'}
    escSimple { esc | "'"}
    escCharSet { esc | "]" | "-"}
    esc  { $[\\\/bfnrtu] }

    whitespace { $[ \n\r\t] }
    //Information about Comment not found in docuentation but was present in javascript grammar
    Comment { "//" ![\n]* }
    Any { "_" }
}

@skip { whitespace | Comment }

@detectDelim
